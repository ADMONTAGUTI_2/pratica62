
import java.util.Collections;
import java.util.List;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica62 {
    public static void main(String[] args) {
        Time t1 = new Time();
        t1.addJogador("Goleiro", new Jogador(1, "Fábio"));
        t1.addJogador("Zagueiro", new Jogador(3, "Manoel"));
        t1.addJogador("Marcador", new Jogador(5, "Jorge"));
        t1.addJogador("Novato", new Jogador(2, "Carlo"));
        t1.addJogador("Técnico", new Jogador(4, "José"));
        t1.addJogador("Meia", new Jogador(6, "Vladimir"));
        t1.addJogador("Meia central", new Jogador(6, "Adriano"));
        
        JogadorComparator comparator = new JogadorComparator(true, true, false);
        List<Jogador> time = t1.ordena(comparator);
        
        time.forEach((j) -> {
            System.out.println(j.toString());
        });
        
        int index = Collections.binarySearch(time, new Jogador(6, "Vladimir"));
        System.out.println(time.toArray()[index]);

//        Set<String> posicoes = t1.getJogadores().keySet();
//        
//        System.out.print("Posição \t Time 1 \t Time 2 \n");
//        for (String p : posicoes) {
//            System.out.print(p + " \t");
//            System.out.print(t1.getJogadores().get(p).toString() + " \t");
//            if (t2.getJogadores().containsKey(p)) {
//                System.out.print(t2.getJogadores().get(p).toString() + "\n");
//            } else {
//                System.out.print("NULL \n");
//            }
//        }
    }
}
