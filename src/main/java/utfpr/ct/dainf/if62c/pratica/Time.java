/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author montaguti
 */
public class Time extends JogadorComparator {
    private HashMap<String, Jogador> jogadores = new HashMap<>();
    
    public HashMap<String, Jogador> getJogadores() {
        return this.jogadores;
    }
    
    public void addJogador(String posicao, Jogador jogador) {
        this.jogadores.put(posicao, jogador);
    }
    
    public List<Jogador> ordena(JogadorComparator j) {
        List<Jogador> list = new ArrayList<Jogador>(this.jogadores.values());
        list.sort(j);
        return list;
    }
    
}
