/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;

/**
 *
 * @author montaguti
 */
public class JogadorComparator implements Comparator<Jogador> {
    
    private final boolean numberOrder;
    private final boolean numberAsc;
    private final boolean nameAsc;
    
    public JogadorComparator() {
        this.numberOrder = true;
        this.numberAsc = true;
        this.nameAsc = true;
    }
    
    public JogadorComparator(boolean arg1, boolean arg2, boolean arg3) {
        this.numberOrder = arg1;
        this.numberAsc = arg2;
        this.nameAsc = arg3;
    }

    @Override
    public int compare(Jogador o1, Jogador o2) {
        if (this.numberOrder) {
            if (this.numberAsc) {
                return o1.numero - o2.numero;
            } else {
                return -(o1.numero - o2.numero);
            }
        } else {
            if (this.nameAsc) {
                return o1.nome.compareTo(o2.nome);
            } else {
                return -(o1.nome.compareTo(o2.nome));
            }
        }
    }
    
}
